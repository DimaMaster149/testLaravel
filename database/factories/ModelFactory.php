<?php

use Faker\Generator as Faker;
use App\Order;
use App\Good;
use App\State;
use App\User;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
	return [
		'user_first_name' => $faker->firstName,
		'user_last_name' => $faker->lastName,
		'user_login' => $faker->unique()->email,
		'password' => bcrypt('secret'),
	];
});

$factory->define(Good::class, function (Faker $faker) {
	return [
		'good_name' => $faker->company,
		'good_price' => $faker->numberBetween(1, 1000),
		'good_advert' => User::all()->random()->id,

	];
});

$factory->define(Order::class, function (Faker $faker) {
	return [
		'order_state' =>State::all()->random()->state_id,
		'order_add_time' => $faker->dateTime($max = 'now'),
		'order_good' => Good::all()->random()->good_id,
		'order_client_phone' => $faker->phoneNumber,
		'order_client_name' => $faker->firstName
	];
});