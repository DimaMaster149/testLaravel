<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
	    //factory(App\User::class, 10)->create();
	    //factory(App\Good::class, 25)->create();
	    factory(App\Order::class, 10)->create();
    }
}
