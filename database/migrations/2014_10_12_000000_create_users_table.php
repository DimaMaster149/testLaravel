<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create('adverts', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('user_first_name');
	        $table->string('user_last_name');
            $table->string('user_login')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('adverts');
    }
}
