@extends('layouts.app')

@section('content')

    <form method="POST" action="/table/order/search" enctype='multipart/form-data'>
        <div class="form-group">
            <label for="order_date_from"> From</label>
            <input type="date" class="form-control" name="order_date_from" value="">
        </div>
        <div class="form-group">
            <label for="order_date_to"> To</label>
            <input type="date" class="form-control" name="order_date_to" value="">
        </div>
        <div class="form-group">
            <label for="order_state"> State</label>
            <input type="text" class="form-control" name="order_state" value="">
        </div>
        <div class="form-group">
            <label for="order_client_phone"> Phone </label>
            <input type="text" class="form-control" name="order_client_phone" value="" >
        </div>
        <div class="form-group">
            <label for="order_client_name"> Name </label>
            <input type="text" class="form-control" name="order_client_name" value="" >
        </div>
        <div class="form-group">
            <label for="order_good"> Good ID </label>
            <input type="number" class="form-control" name="order_good" value="" >
        </div>
        {{csrf_field()}}
        <button type="button" class="btn btn-success search">Search</button>
    </form>

    <table id="myTable" class="display" data-page-length='2'>
        <thead>
        <tr>
            <th>id</th>
            <th>time</th>
            <th>client name</th>
            <th>client phone</th>
            <th>good</th>
            <th>state</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($orders as $order)
            <tr>
                <td>{{$order->order_id}}</td>
                <td>{{$order->order_add_time}}</td>
                <td>{{$order->order_client_name}}</td>
                <td>{{$order->order_client_phone}}</td>
                <td>{{$order->order_good}}
                    <br> {{$order->user_name}} ( {{$order->user_login}} )
                </td>
                <td>{{$order->order_state}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
@section('scripts')
    <script src="/js/app.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#myTable').DataTable();

            $('.search').on('click', function(){
                var order_date_from = $( "input[name='order_date_from']" ).val();
                var order_date_to = $( "input[name='order_date_to']" ).val();
                var order_state = $( "input[name='order_state']" ).val();
                var order_client_phone = $( "input[name='order_client_phone']" ).val();
                var order_client_name = $( "input[name='order_client_name']" ).val();
                var order_good = $( "input[name='order_good']" ).val();
                console.log(order_date_from);
                table.clear().draw();

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/table/order/search',
                    data: {order_date_from:order_date_from, order_date_to:order_date_to, order_state:order_state},order_client_phone:order_client_phone, order_client_name:order_client_name,order_good:order_good,
                    type: 'POST',
                    success: function(data) {
                        console.log(data);
                        if(!$.isEmptyObject(data)) {
                            $.each(data, function(index, value){
                                table.row.add( [
                                    value.order_id ,
                                    value.order_add_time,
                                    value.order_client_name,
                                    value.order_client_phone,
                                    value.order_good,
                                    value.order_state,
                                ]).draw();
                            });
                        }
                    },
                    error: function(e){
                        console.log(e);
                    }
                });
            });
        });
    </script>
@endsection