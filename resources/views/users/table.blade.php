@extends('layouts.app')

@section('content')

    <form method="POST" action="/table/user/search" enctype='multipart/form-data'>
        <div class="form-group">
            <label for="user_first_name"> First name</label>
            <input type="text" class="form-control" name="user_first_name" value="">
        </div>
        <div class="form-group">
            <label for="user_last_name"> Last name</label>
            <input type="text" class="form-control" name="user_last_name" value="">
        </div>
        <div class="form-group">
            <label for="user_login"> Login </label>
            <input type="text" class="form-control" name="user_login" value="" >
        </div>
        {{csrf_field()}}
        <button type="button" class="btn btn-success search">Search</button>
    </form>

    <table id="myTable" class="display" data-page-length='5'>
        <thead>
        <tr>
            <th>user_id</th>
            <th>user_first_name</th>
            <th>user_last_name</th>
            <th>user_login</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($users as $user)
            <tr>
                <td>{{$user->user_id}}</td>
                <td>{{$user->user_first_name}}</td>
                <td>{{$user->user_last_name}}</td>
                <td>{{$user->user_login}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
@section('scripts')
    <script src="/js/app.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#myTable').DataTable();

            $('.search').on('click', function(){
                var user_first_name = $( "input[name='user_first_name']" ).val();
                var user_last_name = $( "input[name='user_last_name']" ).val();
                var user_login = $( "input[name='user_login']" ).val();
                table.clear().draw();

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/table/user/search',
                    data: {user_first_name:user_first_name, user_last_name:user_last_name, user_login:user_login},
                    type: 'POST',
                    success: function(data) {
                        if(!$.isEmptyObject(data)) {
                            $.each(data, function(index, value){
                                table.row.add( [
                                    value.user_id ,
                                    value.user_first_name,
                                    value.user_last_name,
                                    value.user_login,
                                ]).draw();
                            });
                        }
                    },
                    error: function(e){
                        console.log(e);
                    }
                });
            });
        });
    </script>
@endsection
