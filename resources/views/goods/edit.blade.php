@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit</h1>
        <form method="POST" action="/goods/{{$good->good_id}}" enctype='multipart/form-data'>

            <div class="form-group">
                <label for="good_id"> Good ID</label>
                <input type="text" class="form-control" name="good_id" value={{$good->good_id}}>
            </div>
            <div class="form-group">
                <label for="good_name"> Good name</label>
                <input type="text" class="form-control" name="good_name" value={{$good->good_name}}>
            </div>
            <div class="form-group">
                <label for="good_price"> Good price</label>
                <input type="text" class="form-control" name="good_price" value={{$good->good_price}}>
            </div>
            <div class="form-group">
                <label for="good_advert"> Advert </label> <br>
                <select name="good_advert">
                    @foreach($users as $user)
                    <option>{{$user->user_first_name}} {{$user->user_last_name}}/{{$user->user_login}}</option>
                    @endforeach
                    <option selected>{{$good->good_advert_name}}/{{$good->email}} </option>
                </select>
            </div>
            <button type="submit" class="btn btn-success">Edit good</button>
            {{csrf_field() }}
        </form>
    </div>

@endsection