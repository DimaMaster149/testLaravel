@extends('layouts.app')

@section('content')

    <form method="POST" action="/table/good/search" enctype='multipart/form-data'>
        <div class="form-group">
            <label for="good_name"> Good name</label>
            <input type="text" class="form-control" name="good_name" value="">
        </div>
        <div class="form-group">
            <label for="good_price"> Good price</label>
            <input type="text" class="form-control" name="good_price" value="">
        </div>
        <div class="form-group">
            <label for="good_advert"> Advert </label>
            <input type="text" class="form-control" name="good_advert" value="" >
        </div>
        {{csrf_field()}}
        <button type="button" class="btn btn-success search">Search</button>
    </form>

    <table id="myTable" class="display" data-page-length='2'>
        <thead>
        <tr>
            <th>Check</th>
            <th>id</th>
            <th>Good name</th>
            <th>Price</th>
            <th>Advert</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($goods as $good)
            <tr>
                <td><input type="checkbox" checked></td>
                <td>{{$good->good_id}}</td>
                <td><a href="/goods/{{$good->good_id}}/edit" >{{$good->good_name}}</a>
                    <br> Внешний ID: {{$good->good_advert}}
                </td>
                <td>{{$good->good_price}}</td>
                <td>{{$good->good_advert_name}}
                    <br> {{$good->email}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
@section('scripts')
    <script src="/js/app.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#myTable').DataTable();

            $('.search').on('click', function(){
                var good_name = $( "input[name='good_name']" ).val();
                var good_price = $( "input[name='good_price']" ).val();
                var good_advert = $( "input[name='good_advert']" ).val();
                table.clear().draw();

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/table/good/search',
                    data: {good_name:good_name, good_price:good_price, good_advert:good_advert},
                    type: 'POST',
                    success: function(data) {
                        if(!$.isEmptyObject(data)) {
                            console.log(data);
                            $.each(data, function(index, value){
                                table.row.add( [
                                    value.checkbox,
                                    value.good_id,
                                    value.href,
                                    value.good_price,
                                    value.advert_str,
                                ]).draw();
                            });
                        }
                    },
                    error: function(e){
                        console.log(e);
                    }
                });
            });
        });
    </script>
@endsection
