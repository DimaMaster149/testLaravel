<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Пользователи
Route::middleware(['auth'])->group(function () {
	Route::get('/table/user', 'UserController@tableUser')->name('tableUser');
	Route::post('/table/user/search', 'UserController@tableUserSearch')->name('tableUserSearch');

//Заказы
	Route::get('/table/order', 'OrderController@tableOrder')->name('tableOrder');
	Route::post('/table/order/search', 'OrderController@tableOrderSearch')->name('tableOrderSearch');

//Товары
	Route::get('/table/good', 'GoodController@tableGood')->name('tableGood');
	Route::post('/table/good/search', 'GoodController@tableGoodSearch')->name('tableGoodSearch');
//Редактирование товаров
	Route::get('/goods/{good_id?}/edit', 'GoodController@edit')->name('goodEdit');
	Route::post('/goods/{good_id?}', 'GoodController@update')->name('goodUpdate');
	});
