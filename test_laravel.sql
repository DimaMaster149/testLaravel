-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Авг 14 2018 г., 21:13
-- Версия сервера: 5.6.37
-- Версия PHP: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test_laravel`
--

-- --------------------------------------------------------

--
-- Структура таблицы `adverts`
--

CREATE TABLE `adverts` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_login` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `adverts`
--

INSERT INTO `adverts` (`user_id`, `user_first_name`, `user_last_name`, `user_login`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Dima', 'Doe', 'smth@gmail.com', '$2y$10$h8Lcf6Ztnof7L0K/01xxF.3Tgf0TZtV8DwZ46npI9wSijf9CLHWsu', 'GXpzV37ZdFCdlOLcbXPNxBDqBTWEytlHE7YcpqmseIJd3DdR8XBFkQc3n249', '2018-08-14 04:29:27', '2018-08-14 04:29:27'),
(2, 'Kianna', 'Cassin', 'clinton60@anderson.com', '$2y$10$fTGsCVkjU0wtYAzE9ia9OusVrTwQ43yp1xRakyNrXT62Ej5EaU3Py', NULL, '2018-08-14 06:47:15', '2018-08-14 06:47:15'),
(3, 'Dane', 'Sawayn', 'gerhold.jamar@hotmail.com', '$2y$10$vkMlJ/oT2.znKWRpxOfQ5uRIKSUBxkp4ofjbRqcW938jDD.9KcmB.', NULL, '2018-08-14 06:47:15', '2018-08-14 06:47:15'),
(4, 'Kareem', 'Corkery', 'murphy05@fritsch.com', '$2y$10$YRBTqy3igoTMT9U4NNWSSez5nZCHkec84s5ao2VwpEzbqGmgKiY32', NULL, '2018-08-14 06:47:15', '2018-08-14 06:47:15'),
(5, 'Winifred', 'Howell', 'nhane@goodwin.biz', '$2y$10$a4y7YwMDxG/CkxEfIoyKW.tXQPni0rrgttuCq2MkW4.JI7GBSlwkK', NULL, '2018-08-14 06:47:15', '2018-08-14 06:47:15'),
(6, 'Wanda', 'Turcotte', 'elynch@mayer.info', '$2y$10$k474ko4NB2Us2ClJpbnNM.l54FEuHZx7CnOAzfy6MVzgLY5snjjSG', NULL, '2018-08-14 06:47:15', '2018-08-14 06:47:15'),
(7, 'Manley', 'Olson', 'walter.donny@oreilly.org', '$2y$10$fVtMcv6vSmfbPSGGzpyNF.SGj0EtSw2ixzFhPDJgRCMdFrYO50nm2', NULL, '2018-08-14 06:47:15', '2018-08-14 06:47:15'),
(8, 'Jerald', 'Parisian', 'mitchell.rhianna@yahoo.com', '$2y$10$xABoaUmeyh2kIInbnS7O1eUFV/NOo1FGttcrw2yOfc6QPV.y05wga', NULL, '2018-08-14 06:47:15', '2018-08-14 06:47:15'),
(9, 'Lea', 'Moore', 'towne.una@west.com', '$2y$10$692RaVc8XXO3F5OuDpXd0.RKILOh43hk51WoRluDOyklx24X2Asde', NULL, '2018-08-14 06:47:15', '2018-08-14 06:47:15'),
(10, 'Aaliyah', 'Ebert', 'zmiller@yahoo.com', '$2y$10$FUheY1Q2LkEvw3dLleERX.dx3c4VYhJmYMEDHuFWHD89FXa/z/U0G', NULL, '2018-08-14 06:47:15', '2018-08-14 06:47:15'),
(11, 'Raven', 'Hayes', 'orville46@zemlak.com', '$2y$10$nbFAV2kypc3tk5msd745ueCeZdOxTimAJ0.4pnz5vc1Rj5ZaIKgAu', NULL, '2018-08-14 06:47:15', '2018-08-14 06:47:15'),
(12, 'Gustave', 'Pagac', 'gturner@predovic.info', '$2y$10$N5OlGdneZNRwPth4Sf0ZfuY8olH9nEbJfgHxPINNXfS4LMX9XFmOO', NULL, '2018-08-14 06:48:23', '2018-08-14 06:48:23'),
(13, 'Vito', 'Cummerata', 'qquigley@gmail.com', '$2y$10$oESLEqP0pt7okdZkK5eSxOyetdOWtrBIuJbjkyp0QYKXbStv/NrTG', NULL, '2018-08-14 06:48:23', '2018-08-14 06:48:23'),
(14, 'Joel', 'Powlowski', 'ofadel@yahoo.com', '$2y$10$PUAovjL2qdLwYKjLiUW46O3FMwrzO7Txdsx0r1Gret0pnGMtkckUK', NULL, '2018-08-14 06:48:23', '2018-08-14 06:48:23'),
(15, 'Anika', 'Veum', 'eldridge21@hills.info', '$2y$10$mg0fPLS9Vv9TOnr9jHKqxOgdkD9j1zwz.jIi1ZaNYpZESsJ7K4hD.', NULL, '2018-08-14 06:48:23', '2018-08-14 06:48:23'),
(16, 'Felton', 'Greenholt', 'lwiza@yahoo.com', '$2y$10$OKdQcVfriUSBBmccGb8sCuyiyEqGRP4Na.Gf5xtPLOiSCwgQ1057S', NULL, '2018-08-14 06:48:23', '2018-08-14 06:48:23'),
(17, 'Meredith', 'Osinski', 'mustafa71@kertzmann.com', '$2y$10$gdIP7Zlx96yP0RSjNDWBaOwKwGZ7YRX48P3wK4Q9kUmnDTK8C5e1O', NULL, '2018-08-14 06:48:23', '2018-08-14 06:48:23'),
(18, 'Joshuah', 'Turcotte', 'gcrooks@schowalter.com', '$2y$10$0Q4h6MmGhL1lfZu.CTbRpeiJOyuR9TyFyRDO4xGcEqJ3uILuCHiGK', NULL, '2018-08-14 06:48:23', '2018-08-14 06:48:23'),
(19, 'Joany', 'Hayes', 'edna.daugherty@yahoo.com', '$2y$10$/JUvTqzSzBnKOqmkZ2U6o.2hSFReYfc154.1U2z1lDCHEvmYe.6my', NULL, '2018-08-14 06:48:23', '2018-08-14 06:48:23'),
(20, 'Clarissa', 'Rolfson', 'wolff.kennith@stanton.com', '$2y$10$QaWpHhDijx/aNA4Jl9ypYO1k85.e53g0daqyixovLoClokyn0a1TO', NULL, '2018-08-14 06:48:23', '2018-08-14 06:48:23'),
(21, 'Alisha', 'Homenick', 'howard.kshlerin@bechtelar.com', '$2y$10$6CcVsjDgG1g13hz8NqjI3O5F216rbBtvlUgllYOS2jBXFSO0M9vgm', NULL, '2018-08-14 06:48:23', '2018-08-14 06:48:23');

-- --------------------------------------------------------

--
-- Структура таблицы `goods`
--

CREATE TABLE `goods` (
  `good_id` int(10) UNSIGNED NOT NULL,
  `good_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `good_price` int(11) NOT NULL,
  `good_advert` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `goods`
--

INSERT INTO `goods` (`good_id`, `good_name`, `good_price`, `good_advert`, `created_at`, `updated_at`) VALUES
(1, 'Ledner,', 434, 18, '2018-08-14 06:54:01', '2018-08-14 14:48:02'),
(2, 'Rodriguez-Gibson', 371, 21, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(3, 'Pagac Inc', 8, 15, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(4, 'Sipes Ltd', 208, 12, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(5, 'Dibbert-Braun', 898, 13, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(6, 'Fisher LLC', 804, 19, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(7, 'Jerde', 861, 11, '2018-08-14 06:54:01', '2018-08-14 14:09:24'),
(8, 'Zieme-Kshlerin', 788, 18, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(9, 'Waters, Dietrich and Mosciski', 324, 5, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(10, 'Turcotte Inc', 528, 4, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(11, 'Murphy, Dickinson and Feil', 396, 20, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(12, 'Rowe-Ortiz', 117, 8, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(13, 'Cole-Schamberger', 106, 4, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(14, 'Ratke Inc', 828, 12, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(15, 'Leffler-Schowalter', 725, 1, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(16, 'Nienow, Schuster and Dooley', 908, 5, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(17, 'Kuhlman Ltd', 252, 3, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(18, 'Schaefer and Sons', 100, 21, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(19, 'Gulgowski-Shields', 445, 16, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(20, 'Lubowitz-Zieme', 38, 12, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(21, 'Gaylord-Gleichner', 817, 11, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(22, 'VonRueden-Eichmann', 609, 9, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(23, 'Swift, Kutch and Nicolas', 355, 15, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(24, 'Dickens, Jones and Stokes', 544, 18, '2018-08-14 06:54:01', '2018-08-14 06:54:01'),
(25, 'Marvin-Kirlin', 922, 7, '2018-08-14 06:54:01', '2018-08-14 06:54:01');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_08_14_073508_create_goods_table', 2),
(4, '2018_08_14_073633_create_states_table', 2),
(5, '2018_08_14_073941_create_orders_table', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_add_time` datetime NOT NULL,
  `order_good` int(11) NOT NULL,
  `order_client_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_client_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`order_id`, `order_state`, `order_add_time`, `order_good`, `order_client_phone`, `order_client_name`, `created_at`, `updated_at`) VALUES
(1, '1', '2009-08-19 10:09:14', 11, '+14142309479', 'Ignatius', '2018-08-14 06:59:43', '2018-08-14 06:59:43'),
(2, '3', '1983-01-01 16:03:43', 18, '1-585-919-5380 x07590', 'Lea', '2018-08-14 06:59:43', '2018-08-14 06:59:43'),
(3, '3', '2000-04-25 20:20:30', 19, '(539) 723-8644', 'Lucas', '2018-08-14 06:59:43', '2018-08-14 06:59:43'),
(4, '1', '2003-11-04 15:13:47', 3, '1-363-453-2454', 'Brooklyn', '2018-08-14 06:59:43', '2018-08-14 06:59:43'),
(5, '2', '1971-08-23 23:13:40', 9, '736.701.6096', 'Lavina', '2018-08-14 06:59:43', '2018-08-14 06:59:43'),
(6, '2', '2006-01-05 11:04:04', 14, '+1.972.576.6041', 'Augustine', '2018-08-14 06:59:43', '2018-08-14 06:59:43'),
(7, '2', '1983-01-08 10:30:01', 24, '545-452-7865 x1968', 'Haskell', '2018-08-14 06:59:43', '2018-08-14 06:59:43'),
(8, '2', '2015-03-24 08:32:11', 15, '+12906364421', 'Sienna', '2018-08-14 06:59:43', '2018-08-14 06:59:43'),
(9, '3', '1996-05-25 13:09:41', 12, '+13357242599', 'Lonie', '2018-08-14 06:59:43', '2018-08-14 06:59:43'),
(10, '1', '2001-05-04 19:45:16', 11, '526.899.8003', 'Emmanuel', '2018-08-14 06:59:43', '2018-08-14 06:59:43'),
(11, '1', '1994-12-31 05:30:54', 13, '(912) 361-2877', 'Alf', '2018-08-14 14:18:11', '2018-08-14 14:18:11'),
(12, '1', '1976-12-24 00:11:18', 25, '(698) 859-0689', 'Lavina', '2018-08-14 14:18:11', '2018-08-14 14:18:11'),
(13, '1', '1990-09-09 02:03:35', 25, '(946) 220-0371', 'Bridgette', '2018-08-14 14:18:11', '2018-08-14 14:18:11'),
(14, '3', '2018-02-08 18:41:34', 18, '246-830-6190 x59409', 'Muriel', '2018-08-14 14:18:11', '2018-08-14 14:18:11'),
(15, '1', '1972-09-26 17:18:09', 16, '+1-745-507-2580', 'Shayne', '2018-08-14 14:18:11', '2018-08-14 14:18:11'),
(16, '3', '2006-10-10 19:55:00', 13, '762-346-0589', 'Delpha', '2018-08-14 14:18:11', '2018-08-14 14:18:11'),
(17, '2', '1986-06-15 11:23:30', 24, '938-307-6087 x721', 'Estefania', '2018-08-14 14:18:11', '2018-08-14 14:18:11'),
(18, '1', '1973-02-10 04:58:10', 22, '+1-867-985-9141', 'Daphnee', '2018-08-14 14:18:11', '2018-08-14 14:18:11'),
(19, '3', '2007-09-01 00:26:46', 23, '573.282.5923 x6882', 'Marley', '2018-08-14 14:18:11', '2018-08-14 14:18:11'),
(20, '3', '2015-07-24 09:57:50', 13, '+1 (271) 253-8969', 'Lulu', '2018-08-14 14:18:11', '2018-08-14 14:18:11');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `states`
--

CREATE TABLE `states` (
  `state_id` int(10) UNSIGNED NOT NULL,
  `state_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `states`
--

INSERT INTO `states` (`state_id`, `state_name`, `state_slug`, `created_at`, `updated_at`) VALUES
(1, 'Новый', 'new', '2018-08-13 21:00:00', '2018-08-13 21:00:00'),
(2, 'В работе', 'onoperator', '2018-08-13 21:00:00', '2018-08-13 21:00:00'),
(3, 'Подтвержден', 'accepted', '2018-08-13 21:00:00', '2018-08-13 21:00:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `adverts`
--
ALTER TABLE `adverts`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `adverts_user_login_unique` (`user_login`);

--
-- Индексы таблицы `goods`
--
ALTER TABLE `goods`
  ADD PRIMARY KEY (`good_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `adverts`
--
ALTER TABLE `adverts`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблицы `goods`
--
ALTER TABLE `goods`
  MODIFY `good_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `states`
--
ALTER TABLE `states`
  MODIFY `state_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
