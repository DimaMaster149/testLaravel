<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Http\Response;

class UserController extends Controller
{
	//
	public function tableUser() {
		$users = User::orderby('user_id')->get();
		return view('users.table')->with(['users'=>$users]);
	}

	public function tableUserSearch(Request $request) {
		$q = User::query();

		if ($request->user_first_name != '') {
			$q->where('user_first_name', 'like', '%'.$request->user_first_name.'%');
		}
		if ($request->user_last_name != '') {
			$q->where('user_last_name', 'like', '%'.$request->user_last_name.'%');
		}
		if ($request->user_login != '') {
			$q->where('user_login', 'like', '%'.$request->user_login.'%');
		}

		$users = $q->get();
		return Response($users);
	}
}
