<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Good;
use App\User;

use Illuminate\Http\Response;

class GoodController extends Controller
{
	//
	public function edit($id)
	{
		$good = Good::findOrFail($id);
		$users = User::all();
		$user = User::findOrFail($good->good_advert);
		$good->good_advert_name = $user->user_first_name.' '.$user->user_last_name;
		$good->email = $user->user_login;
		return view('goods.edit', compact(['good', 'users']));
	}

	public function update(Request $request, $id)
	{
		$good_advert = $request->input('good_advert');
		$login = explode('/', $good_advert);
		$user = User::where('user_login', $login[1])->first();
		$advert = $user->user_id;

		Good::where('good_id', $id)->update([
			'good_name' =>  $request->input('good_name'),
			'good_price' => $request->input('good_price'),
			'good_advert' => $advert,
		]);
		return redirect('/table/good');
	}


	public function tableGood() {
		$goods = Good::orderby('good_id')->get();
		foreach($goods as $good)
		{
			$user = User::findOrFail($good->good_advert);
			$good->good_advert_name = $user->user_first_name.' '.$user->user_last_name;
			$good->email = $user->user_login;

		}
		return view('goods.table')->with(['goods'=>$goods]);
	}

	public function tableGoodSearch(Request $request) {
		$q = Good::query();

		if ($request->good_name != '') {
			$q->where('good_name', 'like', '%'.$request->good_name.'%');
		}
		if ($request->good_price != '') {
			$q->where('good_price', 'like', '%'.$request->good_price.'%');
		}
		if ($request->good_advert != '') {
			$q->where('good_advert', 'like', '%'.$request->good_advert.'%');
		}

		$goods = $q->get();

		foreach($goods as $good)
		{
			$good->checkbox = '<td><input type="checkbox" checked></td>';
			$good->href = '<a href="/goods/'.$good->good_id.'/edit" >'.$good->good_name.'</a>';

			$user = User::findOrFail($good->good_advert);
			$good->good_advert_name = $user->user_first_name.' '.$user->user_last_name;
			$good->email = $user->user_login;
			$good->advert_str = $good->good_advert_name.'<br>'.$good->email;
		}
		return Response($goods);
	}
}