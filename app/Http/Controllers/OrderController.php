<?php

namespace App\Http\Controllers;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use App\Order;
use App\Good;
use App\State;
use App\User;
use Illuminate\Http\Response;

class OrderController extends Controller
{
	public function tableOrder() {
		$orders = Order::orderby('order_id')->get();
		foreach($orders as $order)
		{
			$state = State::findOrFail($order->order_state);
			$order->order_state = $state->state_name;

			$good = Good::findOrFail($order->order_good);
			$user = User::findOrFail($good->good_advert);
			$order->user_name = $user->user_first_name.' '.$user->user_last_name;
			$order->user_login = $user->user_login;
			$order->order_good = $good->good_name;
		}
		return view('orders.table')->with(['orders'=>$orders]);
	}

	public function tableOrderSearch(Request $request) {
		$q = Order::query();

		if ($request->order_date_from != '') {
			$q->where('order_add_time', '>=', $request->order_date_from);
		}
		if ($request->order_date_to != '') {
			$q->where('order_add_time', '<=', $request->order_date_to);
		}
		if ($request->order_state != '') {
			$q->where('order_state', 'like', '%'.$request->order_state.'%');
		}
		if ($request->order_client_phone != '') {
			$q->where('order_client_phone', 'like', '%'.$request->order_client_phone.'%');
		}
		if ($request->order_client_name != '') {
			$q->where('order_client_name', 'like', '%'.$request->order_client_name.'%');
		}
		if ($request->order_good != '') {
			$q->where('order_good', 'like', '%'.$request->order_good.'%');
		}

		$orders = $q->get();
		foreach($orders as $order)
		{
			$state = State::findOrFail($order->order_state);
			$order->order_state = $state->state_name;

			$good = Good::findOrFail($order->order_good);
			$order->order_good = $good->good_name;
		}
		return Response($orders);
	}
}