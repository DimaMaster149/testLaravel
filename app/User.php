<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_first_name', 'user_last_name', 'user_login', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	/**
	 * The datatable used by the model for authentication.
	 *
	 * @var string
	 */
	protected $table = 'adverts';

	protected $primaryKey = 'user_id';

	public function goods()
	{
		return $this->hasMany('App\Good', 'good_advert', 'user_id');
	}

}
