<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
	protected $fillable = [
		'good_name', 'good_price', 'good_advert',
	];

	public function user()
	{
		return $this->belongsTo('App\User', 'good_advert', 'user_id');
	}

	protected $primaryKey = 'good_id';
}
