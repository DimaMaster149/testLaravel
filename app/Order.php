<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $fillable = [
		'order_state', 'order_add_time', 'order_good', 'order_client_phone', 'order_client_name',
	];

	public function state()
	{
		return $this->hasOne('App\State', 'order_state', 'order_id');
	}

	public function good()
	{
		return $this->hasOne('App\Good', 'order_good', 'order_id');
	}

	protected $primaryKey = 'order_id';
}
